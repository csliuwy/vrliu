# vrliu

#### 介绍
This is the extended version on PC (MS Windows) of the previous Machine Drawing Understanding System (MDUS) developed by Dr. Liu Wenyin (www.cs.cityu.edu.hk/~liuwy) as part of his PhD thesis using the methods published in the following papers. You may select appropriate papers to cite if you wish to use our code.

